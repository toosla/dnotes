#ifndef GTASKLIB_GLOBAL_H
#define GTASKLIB_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(GTASKLIB_LIBRARY)
#  define GTASKLIBSHARED_EXPORT Q_DECL_EXPORT
#else
#  define GTASKLIBSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // GTASKLIB_GLOBAL_H
