#-------------------------------------------------
#
# Project created by QtCreator 2012-01-02T19:52:52
#
#-------------------------------------------------

QMAKE_CXXFLAGS += -std=gnu++0x
QT       += network script

QT       -= gui

TARGET = GTaskLib
TEMPLATE = lib

DESTDIR = "../bin/"

DEFINES += GTASKLIB_LIBRARY

SOURCES += GTaskLib.cpp

HEADERS += GTaskLib.h\
        GTaskLib_global.h

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
