#ifndef GTASKLIB_H
#define GTASKLIB_H

#include "GTaskLib_global.h"

#include <QObject>
#include <QUrl>
#include <QPair>

class GTASKLIBSHARED_EXPORT GTaskLib: public QObject {
    Q_OBJECT

private:
    explicit GTaskLib(QObject* parent=0);
    ~GTaskLib(){}

public:
    static GTaskLib* getInstance();

    QUrl generateLoginLink() const;
    QString getAccessToken(const QString AuthCode) const;

private:
    static GTaskLib* m_ptr;

private:
    const QPair<QString, QString> m_ClientID; // Client ID and Client Secret

};

#endif // GTASKLIB_H
