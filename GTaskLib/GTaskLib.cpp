#include "GTaskLib.h"

#include <QNetworkAccessManager>
#include <QEventLoop>
#include <QNetworkReply>
#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValueIterator>

GTaskLib* GTaskLib::m_ptr = NULL;

GTaskLib *GTaskLib::getInstance()
{
    if (!m_ptr)
        m_ptr = new GTaskLib;
    return m_ptr;
}

QUrl GTaskLib::generateLoginLink() const
{
    return QUrl("https://accounts.google.com/o/oauth2/auth?" + m_ClientID.first + "&redirect_uri=urn:ietf:wg:oauth:2.0:oob&scope=https://www.googleapis.com/auth/tasks&response_type=code");
}

QString GTaskLib::getAccessToken(const QString AuthCode) const
{
    //getting short-lived access token
    QNetworkAccessManager netman;
    QEventLoop loop;
    QNetworkReply *reply;
    QByteArray post_params;
    QNetworkRequest request;
    QUrl params;

    request.setUrl(QUrl("https://accounts.google.com/o/oauth2/token"));
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");

//TODO: "replace commented code below with QUrlQuery according to qt5";
//    params.addQueryItem("client_id", m_ClientID.first);
//    params.addQueryItem("client_secret", m_ClientID.second);
//    params.addQueryItem("code", AuthCode);
//    params.addQueryItem("redirect_uri", "urn:ietf:wg:oauth:2.0:oob");
//    params.addQueryItem("grant_type", "authorization_code");

//    post_params = params.encodedQuery();

    reply = netman.post(request, post_params);

    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply, SIGNAL(sslErrors(QList<QSslError>)), reply, SLOT(ignoreSslErrors()));
    loop.exec();

    reply->deleteLater();

    QByteArray result = reply->readAll();
    QScriptValue scriptVal;
    QScriptEngine scriptEng;

    scriptVal = scriptEng.evaluate("("+ QString(result) +")");

    return scriptVal.property("access_token").toString();
}

GTaskLib::GTaskLib(QObject *parent):
    QObject(parent),
    m_ClientID("854929325510.apps.googleusercontent.com", "Vimgb67jOqg0YQepSOj8dStP")
{
}
