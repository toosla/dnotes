#include <QtWidgets/QApplication>
#include <QTime>
#include <QTranslator>

#include "MainFacade.h"
#include "Utils/RunGuard.h"
#include "defs.h"

int main(int argc, char *argv[])
{
    RunGuard guard( "SKLAHFLKJ)(*)@LKSJF(FMLA:UFKA:SFJA" );
    if ( !guard.tryToRun() )
    {
        DEB << "Another instance of the App is running, closing";
        return 0;
    }

    QApplication app(argc, argv);
    app.setQuitOnLastWindowClosed(false);

    QTranslator translator;
    bool translLoaded = translator.load("app_" + QLocale::system().name(), "../src/locale");

    Q_ASSERT( translLoaded );
    app.installTranslator(&translator);

    MainFacade facade;
    facade.createTrayCotrol();
    facade.initializeAppEngines();
    
    qsrand(QTime::currentTime().msec());

    return app.exec();
}
