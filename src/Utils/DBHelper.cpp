#include "DBHelper.h"
#include "View/Note.h"

//------------------------------------------------------------------------------------------------
bool DBHelper::CreateNotesTable()
{
    QSqlQuery q("CREATE TABLE IF NOT EXISTS " + QString(notesTableName) + "("
                "ID INTEGER PRIMARY KEY   AUTOINCREMENT,"
                "CreationDate TEXT,"
                "Text TEXT,"
                "Loc_X INTEGER,"
                "Loc_Y INTEGER,"
                "Height INTEGER,"
                "Width INTEGER,"
                "Hidden BOOOLEAN,"
                "Skin TEXT);");

    if(!q.isActive())
    {
        DEB << q.lastError();
        Q_ASSERT(!"Couldn't create the table for Notes");
    }

    return q.isActive();
}

bool DBHelper::CreatePrefsTable()
{
    QSqlQuery q("CREATE TABLE IF NOT EXISTS " + QString(prefsTableName) + "(" \
                "Name TEXT UNIQUE NOT NULL," //preference name
                "IntValue INTEGER DEFAULT NULL," //value type variations ...
                "RealValue REAL DEFAULT NULL,"
                "TxtValue TEXT DEFAULT NULL);");

    if(!q.isActive())
    {
        DEB << q.lastError();
        Q_ASSERT(!"Couldn't create the Preference table");
    }

    return q.isActive();
}

bool DBHelper::CreateRemovedNotesTable()
{
    //TODO: Maybe just copy structure from the Notes table in case MySql supports that
    return false;
}

bool DBHelper::RemoveNoteWithUID(quint64 uid)
{
    if( uid <= 0 )
        return false;

    QSqlQuery q("DELETE FROM "+ QString(notesTableName ) + " WHERE ID = " + QString::number(uid));

    if(!q.isActive())
    {
        DEB << q.lastError();
        Q_ASSERT(!"Couldn't remove the element from the table!");
    }else
        DEB << "Removed element from the table with ID: " + QString::number(uid);

    return q.isActive();
}

bool DBHelper::UpdateOrInsertNote(Note *note, bool doUpdating)
{
    QSqlQuery q;
    if( doUpdating )
    {
        q.prepare( "UPDATE " +  QString(notesTableName) + \
                   " SET CreationDate = :date, Text = :text, Loc_X = :loc_x, Loc_Y = :loc_y," \
                   " Height = :size_h, Width = :size_w, Hidden = :hidden, Skin = :skinName" \
                   " WHERE ID = " + QString::number( note->getUID() ));
    }else{
        q.prepare( "INSERT INTO "+ QString(notesTableName) +
                   " VALUES (NULL, :date, :text, :loc_x, :loc_y, :size_h, :size_w, :hidden, :skinName)");
    }

    q.bindValue(":date", note->getDate() );
    q.bindValue(":text", note->getNoteText() );
    q.bindValue(":loc_x", note->getNotePos().x() );
    q.bindValue(":loc_y", note->getNotePos().y() );
    q.bindValue(":size_h", note->size().height() );
    q.bindValue(":size_w", note->size().width() );
    q.bindValue(":hidden", note->isHidden() );
    q.bindValue(":skinName", note->getSkinName() );

    if( !q.exec() )
    {
        DEB << q.lastError();
        Q_ASSERT(!"Got SQL error while saving notes");
    }else{
        if(!note->getUID())
            note->setUID( q.lastInsertId().toLongLong() );
    }

    return q.isActive();
}

bool DBHelper::AddJustRemovedNote(const Note *)
{
    //TODO: Implement. Need to make it to function as a stack
}

bool DBHelper::UpdatePref(QString prefName, qint64 *intVal, qreal *realVal, QString *txtVal)
{
    //There must the only passed Value argument
    {
        void* argArr[] =  { intVal, realVal, txtVal };
        bool onlyOneNotNullArg= std::count(argArr, argArr + sizeof(argArr)/sizeof(argArr[0]), nullptr) == sizeof(argArr)/sizeof(argArr[0]) - 1;
        Q_ASSERT( onlyOneNotNullArg );
        if( !onlyOneNotNullArg )
            return false;
    }

    bool exists = false;
    QSqlQuery select_q( "SELECT Name FROM " + QString( prefsTableName ) +
                        " WHERE Name = '" + prefName + "'");

    if( select_q.isActive() )
    {
        exists = select_q.first();
    }else{
        DEB << select_q.lastError();
        Q_ASSERT(!"Got SQL error while saving prefs");
        return false;
    }

    QString valStr;
    QVariant theVal;
    //deducing the value we want to save/update
    {
        if( intVal )
        {
            valStr = "IntValue";
            theVal.setValue<qint64>(*intVal);
        }
        else if ( realVal )
        {
            valStr = "RealValue";
            theVal.setValue<qreal>(*realVal);
        }
        else
        {
            valStr = "TxtValue";
            theVal.setValue<QString>(*txtVal);
        }
    }

    QSqlQuery upd_q;
    if( exists )
    {
        //updating
        upd_q.prepare(  "UPDATE " + QString(prefsTableName) + " SET "+valStr+" = :value"
                       " WHERE Name = :pref;" );
    }else{
        upd_q.prepare(  "INSERT INTO " + QString(prefsTableName) +
                       " ( Name,"+ valStr +")"
                       " VALUES(:pref, :value );" );
    }
//        upd_q.bindValue(":valCol", valStr);        //Value column name
    upd_q.bindValue(":pref", prefName ); //Row name
    upd_q.bindValue(":value", theVal );

//        DEB << "valStr - " << upd_q.boundValues();
//        DEB << upd_q.lastQuery();

    if( !upd_q.exec() )
    {
        DEB << upd_q.lastError();
        Q_ASSERT(!"Got SQL error while saving prefs");
    }else{
        DEB << "Preference " + prefName + " successfully saved";
    }

    return upd_q.isActive();
}

