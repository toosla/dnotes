#ifndef DBHELPER_H
#define DBHELPER_H

#pragma once

#include <typeinfo>
#include <QtSql>

#include "defs.h"

static constexpr char notesTableName[] = "Notes";
static constexpr char prefsTableName[] = "Preferences";
static constexpr char removedTableName[] = "RemovedNotes";

class Note;

class DBHelper
{
public:
    DBHelper() = delete;
    Q_DISABLE_COPY(DBHelper)

    static bool CreateNotesTable();
    static bool CreatePrefsTable();
    static bool CreateRemovedNotesTable();
    static bool RemoveNoteWithUID( quint64 uid );
    static bool UpdateOrInsertNote( Note* note, bool doUpdating /*otherwise Inserting*/ );
    static bool AddJustRemovedNote( const Note* );
    //Function does not take ownership on passed Args
    static bool UpdatePref( QString prefName, qint64* intVal, qreal* realVal, QString* txtVal );

    template <typename T>
    static bool fetchPref( QString prefName, T& outValue )
    {
        QString valStr;
        //deducing the value we want to fetch
        if( typeid(T) == typeid(qint64) )
            valStr = "IntValue";
        else if( typeid(T) == typeid(qreal) )
            valStr = "RealValue";
        else if ( typeid(T) == typeid(QString) )
            valStr = "TxtValue";
        else
        {
            Q_ASSERT(!"Not allowed data type used!");
            return false;
        }

        QSqlQuery select_q( "SELECT " + valStr + " FROM " + QString( prefsTableName ) +
                            " WHERE Name = '" + prefName + "'");

        if( select_q.isActive() )
        {
            if(select_q.first()){
                outValue = select_q.value(0).value<T>();
                return true;
            }
        }else{
            DEB << select_q.lastError();
            Q_ASSERT(!"Got SQL error while fetching prefs");
        }
        return false;
    }
};

#endif // DBHELPER_H
