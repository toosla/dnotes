#include "Skin.h"
#include "../defs.h"

#include <QFileInfo>
#include <QMovie>

Skin::Skin(QDir folder):
    m_folder(folder)
{
    m_name = m_folder.dirName();
    
    m_image_aliases << "TopL" << "TopC" << "TopR" << "CenL" << "Cen" << "CenR" << "BtmL" << "BtmC" << "BtmR";    
    
    if (m_name == SkinNames::Default)
        CreateDefaultSkin(); 
    else
        LoadResources();
}

Skin::~Skin()
{
    foreach(Image *img, m_images){
        delete img;
    }
    
    DEB << "Skin - " + m_name + " destroyed";
}

QString Skin::GetStyle() const
{
    return m_styleSheet;
}

bool Skin::UseFixedSize() const
{
    return !(m_fixed_rect.isEmpty());
}

void Skin::LoadResources()
{
    Q_ASSERT(m_folder.exists());
    
    foreach (QFileInfo inf, m_folder.entryInfoList(QDir::Files | QDir::NoDotAndDotDot)) {
        QString file_name = inf.fileName();
        QString base_name = inf.baseName();
        
        // stylesheet
        if( file_name == "style.css"){ 
            QFile *skinFile = new QFile(inf.absoluteFilePath());
            if(skinFile->open(QIODevice::ReadOnly | QIODevice::Truncate | QIODevice::Text)){
                m_styleSheet = skinFile->readAll();
                skinFile->close();
                skinFile->deleteLater();
            }else{
                DEB << "Couldn't read css file";
                Q_ASSERT(false);
            }
        // background image
        }else if(file_name == "bg.png" || file_name == "bg.jpg"){
            m_backGroundImg = QImage(inf.absoluteFilePath());
            m_fixed_rect = m_backGroundImg.size();
        }
        // other images
        else if (isAdditionalImage(base_name)){
            m_images.insert(m_image_aliases.indexOf(base_name), (new Image(inf.absoluteFilePath())));
        }
    
        else if(true){
            
        }
    }
}

void Skin::CreateDefaultSkin()
{
    QFile skinFile(":/Skins/default.css");
    
    // stylesheet
    if(skinFile.open(QIODevice::ReadOnly | QIODevice::Truncate | QIODevice::Text)){
        m_styleSheet = skinFile.readAll();
        skinFile.close();
        skinFile.deleteLater();
    }
}

bool Skin::isAdditionalImage(QString fileName) const
{
    return m_image_aliases.contains(fileName);
}
