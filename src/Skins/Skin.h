#ifndef SKIN_H
#define SKIN_H

// --------------------------------------------
// Class for storing all info about certain skin
// --------------------------------------------

#include <QIcon>
#include <QImage>
#include <QFont>
#include <QDir>
#include <QMap>

#include "Image.h"

class Skin
{
public:
    Skin(QDir folder);
    ~Skin();

// members
public:    
    enum POS{
        TOP_LEFT,
        TOP_CENTER,
        TOP_RIGHT,
        CENTER_LEFT,
        CENTER,
        CENTER_RIGHT,
        BTM_LEFT,
        BTM_CENTER,
        BTM_RIGHT
    };
    
private:
    QDir m_folder;
    QString m_name;
    QString m_styleSheet;
    QIcon m_closeBtn;
    QImage m_backGroundImg;
    QMap <int, Image*> m_images;
    QSize m_fixed_rect;
    QFont m_font_main;
    QFont m_font_date;
    QStringList m_image_aliases;
    
// methods
public:
    QString GetName() const { return m_name; }
    QString GetStyle() const;
    const QImage& GetBackgroundImage() const { return m_backGroundImg; }
    const QMap<int, Image*>& GetImages() const { return m_images; }
    bool UseFixedSize() const;
    QSize GetSize() const { return m_fixed_rect; }
    QSize GetMaxSize() const { return {800,800}; }
private:
    void LoadResources();
    void CreateDefaultSkin();
    bool isAdditionalImage(QString fileName) const;
};

#endif // SKIN_H
