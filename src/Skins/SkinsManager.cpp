#include "SkinsManager.h"
#include "../defs.h"

#include <QtWidgets/QApplication>


SkinsManager* SkinsManager::m_self = NULL;

SkinsManager::SkinsManager(QObject *parent) :
    QObject(parent)
{
    m_skins_dir = QDir(qApp->applicationDirPath() + "/skins");
    Q_ASSERT(m_skins_dir.exists());
}

SkinsManager *SkinsManager::GetInst()
{
    if (!m_self){
        m_self = new SkinsManager;
    }
    
    return m_self;
}

SkinPtr SkinsManager::GetCurrCommonSkin() const
{
    return SkinPtr(m_currentSkin);
}

SkinPtr SkinsManager::GetSkinByName(QString name) const
{
    //TODO: verify it works correctly
    SkinPtr sharedPtr;
    if( !mSkinsWeakMap.contains(name) || !mSkinsWeakMap[name] )
    {
        sharedPtr.reset( LoadSkin(name) );
        Q_ASSERT( sharedPtr );
        mSkinsWeakMap[name] = sharedPtr;
    }

    return mSkinsWeakMap[name].toStrongRef();
}

QString SkinsManager::GetCurrCommonSkinName() const
{
    Q_ASSERT(!m_currentSkin.isNull());
    return m_currentSkin->GetName();
}

QStringList SkinsManager::GetAvailableSkinsList() const
{
    QStringList listOfFiles = m_skins_dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
    listOfFiles.push_front("default");
    return listOfFiles;
}

Skin *SkinsManager::LoadSkin(QString name) const
{
    QDir skin_dir(m_skins_dir.absolutePath() + "/" + name);
    if( !skin_dir.exists() && name != SkinNames::Default )
    {
        skin_dir.setPath(QString(SkinNames::Default));
        DEB << "Couldn't load specified skin";
    }
    return (new Skin(skin_dir));
}

void SkinsManager::SetCurrCommonSkin(QString name)
{
    Q_ASSERT(!name.isEmpty());
    
    QString currSkinName = m_currentSkin ? GetCurrCommonSkinName() : "";
    if (m_currentSkin && currSkinName == name){
        DEB << "Curr. Skin is already set as - " + name;
    }else{
        m_currentSkin = SkinPtr(LoadSkin(name));
        Q_ASSERT(!m_currentSkin.isNull());
        
        emit CommonSkinChanged(currSkinName, m_currentSkin);
    }
}
