#ifndef IMAGE_H
#define IMAGE_H

#include <QImage>
#include <QTimer>
#include <QList>

class Image
{
public:
    Image();
    Image(QString f_path);
    ~Image();
    
private:
    bool m_is_null;
    QString m_filePth;
    QTimer m_timer;
    QImage m_img;
    QList<QImage> m_frames;
    int m_curFrame;
    
public:
    bool isNull() const;
    const QImage &GetImage() const;
    inline const QTimer *getUpdTimer() const { return &m_timer; }
};

#endif // IMAGE_H
