#include "Image.h"

#include <QImageReader>
#include <QObject>

#include <QDebug>

Image::Image():
    m_is_null(true)
{
}

Image::Image(QString f_path):
    m_is_null(false),
    m_filePth(f_path),
    m_curFrame(0)
{
    QImageReader imReader(f_path);
    
    if (!imReader.canRead()){
        Q_ASSERT(false);
        return;
    }
    
    if (imReader.supportsAnimation()){
        Q_ASSERT(imReader.imageCount());
        Q_ASSERT(imReader.nextImageDelay() > 0);
        
        // reading all frames from image
        while( imReader.canRead() ){
            m_frames.append(imReader.read());
        }
        
        Q_ASSERT(!m_frames.isEmpty());
        
        if (!m_frames.isEmpty()){
            //simulate animation
            QObject::connect(&m_timer, &QTimer::timeout, [this](){
                m_curFrame++;
            });
            
            m_timer.setInterval(imReader.nextImageDelay());
            m_timer.start();
        }
    }else{
        m_img = QImage(f_path);
    }
}

Image::~Image()
{
    qDebug() << "Destroyed picture - " + m_filePth;
}

bool Image::isNull() const
{
    return (m_is_null || GetImage().isNull());
}

const QImage& Image::GetImage() const
{
    if (m_frames.isEmpty()){
        return m_img;
    }else{
        auto imgCount = m_frames.size();
        return m_frames.at(m_curFrame % imgCount);
    }
}

