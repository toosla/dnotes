#ifndef SKINSMANAGER_H
#define SKINSMANAGER_H

#include <QObject>
#include <QSharedPointer>
#include <QDir>

#include "Skin.h"

typedef QSharedPointer<Skin> SkinPtr;

class SkinsManager : public QObject
{
private:
    Q_OBJECT
    Q_DISABLE_COPY(SkinsManager)
    Q_CLASSINFO("Role", "Manages skins creating/deleting process and gives access to skins")
    explicit SkinsManager(QObject *parent = 0);

    static SkinsManager *m_self;
    SkinPtr m_currentSkin;
    QDir m_skins_dir;
    
public:
    static SkinsManager* GetInst();
    
    SkinPtr GetCurrCommonSkin() const;
    SkinPtr GetSkinByName( QString name ) const;
    QString GetCurrCommonSkinName() const;
    inline QDir GetSkinsDir() const {return m_skins_dir;}

    QStringList GetAvailableSkinsList() const;

public slots:
    void SetCurrCommonSkin(QString name);
signals:
    void CommonSkinChanged(QString, SkinPtr);

protected:
    Skin* LoadSkin(QString name) const;

private:
    mutable QMap<QString, QWeakPointer<Skin>> mSkinsWeakMap;
};

#endif // SKINSMANAGER_H
