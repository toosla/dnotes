#include "EditField.h"

#include "Note.h"
#include "defs.h"

#include <QtWidgets/QApplication>
#include <QtWidgets/QGraphicsOpacityEffect>
#include <QBrush>
#include <QtWidgets/QDesktopWidget>
#include <QMouseEvent>
#include <QPainter>
#include <QMenu>
#include <functional>

//image used to draw resizing button
static QImage cResizeBtnImg;

EditField::EditField( QWidget *parent ): QTextEdit(parent)
{
    Init();
}

EditField::EditField( const QString &text, QWidget *parent ): QTextEdit(parent)
{
    setText(text);
    Init();
}

void EditField::Init()
{
    if( cResizeBtnImg.isNull() )
        cResizeBtnImg.load(":/images/resize.png");
    Q_ASSERT( !cResizeBtnImg.isNull() );

    m_bMidBtnPressed = false;
    m_dragResizingEnabled = false;
    m_resizeCursorActive = false;

    setMouseTracking(true);

    //context menu policy
    setContextMenuPolicy( Qt::CustomContextMenu );

    connect(this, &EditField::customContextMenuRequested, this, &EditField::ShowContextMenu );
}

QRect EditField::GetResizeBtnRect() const
{
    QRect rect;
    rect.setTopLeft( QPoint(viewport()->width() - cResizeBtnImg.width(),
                            viewport()->height() - cResizeBtnImg.height() )
                    );
    rect.setWidth( cResizeBtnImg.width() );
    rect.setHeight( cResizeBtnImg.height() );

    return rect;
}

void EditField::mousePressEvent(QMouseEvent *e)
{
    
    if(e->button() == Qt::MidButton){
        m_bMidBtnPressed = true;
    }else if(e->button() == Qt::LeftButton){
        //resizing button
        if( GetResizeBtnRect().contains( e->localPos().toPoint() ) &&
            supportsResizing()
          )
        {
            m_dragResizingEnabled = true;
            DEB << "Start resizing";
        }
    }

    QTextEdit::mousePressEvent(e);
}

void EditField::mouseReleaseEvent(QMouseEvent *e)
{
    if(e->button() == Qt::MidButton){
        m_bMidBtnPressed = false;
        m_prev_pos.setX(0); m_prev_pos.setY(0);
    }else if(e->button() == Qt::LeftButton){
        //resizing button
        if( m_dragResizingEnabled ){
            m_dragResizingEnabled = false;
            emit resized();
            DEB << "End resizing";
        }
    }
}

void EditField::mouseMoveEvent(QMouseEvent *e)
{
    //moving note
    if(m_bMidBtnPressed){
        if (m_prev_pos.isNull()){
            m_prev_pos = e->globalPos();
        }
        
        QPoint dPos = e->globalPos() - m_prev_pos;
        QRect newWndPos(parentWidget()->pos() + dPos, parentWidget()->geometry().bottomRight() + dPos);
        const auto& desktop = qApp->desktop();
        auto desktopRect = desktop->screenGeometry(desktop->screenNumber(this));
        
        auto isFromAnyScreeen = [&desktop](const QRect& rect)->bool{
            
            // whole note is on one of the screens
            for( auto i = 0; i < desktop->screenCount(); i++ ){
                auto scrRect = desktop->screenGeometry(i);
                if ( scrRect.contains(rect) )
                    return true;
            }            
            
            //both sides are on any screen
            for( auto i = 0; i < desktop->screenCount(); i++ ){
                auto scrRect = desktop->screenGeometry(i);
                if ( 
                        !(scrRect.contains(rect.topLeft()) && scrRect.contains(rect.bottomLeft()))
                        && !(scrRect.contains(rect.topRight()) && scrRect.contains(rect.bottomRight()))
                        )
                    return false;
            }
            return true;
        };
        
        // is note is inside screen
        if (isFromAnyScreeen(newWndPos)){
            m_prev_pos = e->globalPos(); 
            emit sendCoord(newWndPos.topLeft());
        }else if ( newWndPos.left() > 0 && newWndPos.right() < desktopRect.width() ){
            m_prev_pos = e->globalPos(); 
            emit sendCoord(newWndPos.topLeft() - QPoint(0, dPos.y()));
        }else if ( newWndPos.top() > 0 && newWndPos.bottom() < desktopRect.height() ){    
            m_prev_pos = e->globalPos(); 
            emit sendCoord(newWndPos.topLeft() - QPoint(dPos.x(), 0));
        }
        return;
    }
    //resizing
    else if( m_dragResizingEnabled && supportsResizing() ){
        auto parentGeom = parentWidget()->geometry();
        QPoint dSize = QPoint(parentGeom.width(), parentGeom.height()) -
                geometry().bottomRight() +
                QPoint(cResizeBtnImg.width() , cResizeBtnImg.height() );
        QRect newGeometry(parentGeom.topLeft(), e->globalPos() + dSize );

        emit resizeNote( newGeometry );
    }
    
    //over resizing button
    if( !m_skin->UseFixedSize() && GetResizeBtnRect().contains( e->localPos().toPoint() ) )
    {
        if(!m_resizeCursorActive)
        {
            m_resizeCursorActive = true;
#ifdef Q_OS_LINUX
            QGuiApplication::setOverrideCursor(QCursor(Qt::SizeFDiagCursor));
#else
            QGuiApplication::setOverrideCursor(QCursor(Qt::SizeAllCursor));
#endif
            DEB << "Activated resizing cursor";
        }
    }
    else if( m_resizeCursorActive )
    {
        m_resizeCursorActive = false;
        QGuiApplication::restoreOverrideCursor();
        DEB << "Deactivated resizing cursor";
    }

    QTextEdit::mouseMoveEvent(e);
}

void EditField::leaveEvent(QEvent * event)
{
    Q_UNUSED( event );

    if( m_resizeCursorActive )
    {
        m_resizeCursorActive = false;
        QGuiApplication::restoreOverrideCursor();
        DEB << "Deactivated resizing cursor";
    }
}

void EditField::focusOutEvent(QFocusEvent *e)
{
    auto note = qobject_cast<Note*>(parentWidget());
    if (note)
        note->decVisibleLevel();
    else
        DEB << "Couldn't cast to Note*";

    if( m_resizeCursorActive )
    {
        m_resizeCursorActive = false;
        QGuiApplication::restoreOverrideCursor();
        DEB << "Deactivated resizing cursor";
    }
    
    QTextEdit::focusOutEvent(e);
}

void EditField::paintEvent(QPaintEvent *event)
{
    QPainter painter(viewport());

    //skin drawing
    if (m_skin){
         // background image
         if (!m_skin->GetBackgroundImage().isNull())
             painter.drawImage(QPoint(0, 0), m_skin->GetBackgroundImage());

         // rest images
         foreach(int i, m_skin->GetImages().keys()){
             const QImage& img = m_skin->GetImages().value(i)->GetImage();
             auto img_size = img.size();

             QPoint place;
             int w = viewport()->width();
             int h = viewport()->height();

             switch (i){
             case Skin::TOP_LEFT:
                 place = QPoint(0, 0);
                 break;
             case Skin::TOP_CENTER:
                 place = QPoint(w/2 - img_size.width()/2, 0);
                 break;
             case Skin::TOP_RIGHT:
                 place = QPoint(w - img_size.width(), 0);
                 break;
             case Skin::CENTER_LEFT:
                 place = QPoint(0, h/2 - img_size.height()/2);
                 break;
             case Skin::CENTER:
                 place = QPoint(w/2 - img_size.width()/2, h/2 - img_size.height()/2);
                 break;
             case Skin::CENTER_RIGHT:
                 place = QPoint(w - img_size.width(), h/2 - img_size.height()/2);
                 break;
             case Skin::BTM_RIGHT:
                 place = QPoint(w - img_size.width(), h - img_size.height());
                 break;
             case Skin::BTM_CENTER:
                 place = QPoint(w/2 - img_size.width()/2, h - img_size.height());
                 break;
             case Skin::BTM_LEFT:
                 place = QPoint(0, h - img_size.height());
                 break;
             default:
                 DEB << "Image position unrecognized!";
                 Q_ASSERT(false);
             }

             painter.drawImage(place, img);
         }
     }
     
    //resizing button
    if( supportsResizing() )
        painter.drawImage( GetResizeBtnRect().topLeft(), cResizeBtnImg );

    QTextEdit::paintEvent(event);
}


void EditField::setSkin(SkinPtr ptr)
{
    m_skin = ptr;
}

void EditField::ShowContextMenu( const QPoint& pos )
{
    QPoint globalPos = this->mapToGlobal(pos);
    QScopedPointer<QMenu> menu(this->createStandardContextMenu(pos));

    // -------------------------------------
    menu->addSeparator();
    // -------------------------------------
    // Hide
    menu->addAction("Hide", parent(), SLOT(smoothHide()), Qt::CTRL + Qt::Key_H);
    // Close
    QAction* closeAct = menu->addAction("Close");
    closeAct->setShortcut(Qt::CTRL + Qt::Key_W);
    connect(closeAct, &QAction::triggered, [this](){
        QTimer::singleShot( 0, this->parent(), SLOT(close()) );
    });
    // -------------------------------------
    menu->addSeparator();
    // -------------------------------------
    QMenu* skinsSubMenu = menu->addMenu("Skin");
    //populating skins sub menu
    {
        auto skinsList = SkinsManager::GetInst()->GetAvailableSkinsList();
        for( const auto& skinName : skinsList ){
            connect( skinsSubMenu->addAction(skinName), &QAction::triggered, [this, skinName]{
                Note* note = qobject_cast<Note*>(this->parent());
                if( note->getSkinName() != skinName )
                    note->setSkin( SkinsManager::GetInst()->GetSkinByName( skinName ) );
            });
        }
    }

    //TODO : Finish me!
    menu->addAction("Due Date");
    menu->addAction("Priority"); // TODO: Rename me (feature will be to draw marks on BG)

    menu->exec(globalPos);
}

bool EditField::supportsResizing() const
{
   return !m_skin || !m_skin->UseFixedSize();
}
