#ifndef PROPERTIES_H
#define PROPERTIES_H

#include <QtWidgets/QWidget>

class QSettings;
class QComboBox;
class QSlider;
class GooLogin;

class Properties : public QWidget
{
    Q_OBJECT
public:
    Properties(QWidget *parent = 0);

signals:
    void opacityChanged(qreal);
    void skinChanged(QString);

public slots:

private slots:
    void skinChange(const QString);
    void opacityChange();
    void discardChanges();
    void saveChanges_and_close();

private: //metods
    void init();
    void readSettings();
private: //members
    QComboBox *skinCmbBox;
    QSlider *opacitySlider;
    GooLogin *m_webView;
protected:
    virtual void closeEvent(QCloseEvent *);
};

#endif // PROPERTIES_H
