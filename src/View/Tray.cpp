#include "Tray.h"
#include "Google/Gtask.h"

#include <QtWidgets/QMenu>
#include <QtWidgets/QAction>
#include <QLabel>
#include <QIcon>

#include "NotesEng/NotesController.h"

#include "defs.h"

#define nCntrl NotesController::getInstance()

Tray::Tray(QObject *parent) :
    QSystemTrayIcon(parent)
{
    setUpTray();
}

void Tray::setUpTray(){ 
    QMenu *NotesActMenu = new QMenu(trUtf8("LOC_NOTES")); //  "Notes->" menu setup
    //BUG: Below items won't show upon Ubuntu
    QAction *restoreAllAct = NotesActMenu->addAction(trUtf8("LOC_RESTORE_ALL"));
    QAction *hideAllAct = NotesActMenu->addAction(trUtf8("LOC_HIDE_ALL"));
    QAction *closeAllAct = NotesActMenu->addAction(trUtf8("LOC_CLOSE_ALL"));

    QMenu *HelpMenu= new QMenu(trUtf8("LOC_HELP"));

    m_trayIconMenu = new QMenu; 

    QAction *addNoteAct = m_trayIconMenu->addAction(trUtf8("LOC_CREATE_NEW"));
    m_trayIconMenu->addMenu(NotesActMenu);
    QAction* settingsAct = m_trayIconMenu->addAction(trUtf8("LOC_PREFS"));
    m_trayIconMenu->addSeparator();
    m_trayIconMenu->addMenu(HelpMenu);
    m_trayIconMenu->addSeparator();
    QAction* quitAct = m_trayIconMenu->addAction(trUtf8("LOC_QUIT"));

    // notes count lable
    m_notes_count_lbl = new QLabel("", m_trayIconMenu);
    m_notes_count_lbl->setGeometry(QRect(3, 3, 25, 20));
    m_notes_count_lbl->setAlignment(Qt::AlignTop | Qt::AlignHCenter);
//    m_notes_count_lbl->setStyleSheet("font-weight: bold;");
    
    setContextMenu(m_trayIconMenu);
    m_trayIconMenu->setDefaultAction(addNoteAct);
    
    setIcon(QIcon(":/images/TrayIcon.png"));
    
    connect(quitAct,       &QAction::triggered, nCntrl, &NotesController::onExit);
    connect(addNoteAct,    &QAction::triggered, nCntrl, &NotesController::addNote);
    connect(hideAllAct,    &QAction::triggered, nCntrl, &NotesController::hideAll);
    connect(restoreAllAct, &QAction::triggered, nCntrl, &NotesController::restoreAll);
    connect(settingsAct,   &QAction::triggered, nCntrl, &NotesController::on_settingsShow);
    connect(closeAllAct,   &QAction::triggered, nCntrl, &NotesController::closeAll);
    
    connect(this, &Tray::activated, this, &Tray::on_Tray_activated );
    connect(nCntrl, &NotesController::notesQuantityChanged, this, &Tray::on_UpdateNotesCount);
}

//NOTE: This doesnt work on ubuntu, activated signal never sent no matter what you do. Need a way to workaround
void Tray::on_Tray_activated(QSystemTrayIcon::ActivationReason reason)
{
    switch(reason){
    case QSystemTrayIcon::Trigger:
        nCntrl->setAllNotesTopmost();
        break;
    case QSystemTrayIcon::DoubleClick:
        nCntrl->addNote();
        break;
    case QSystemTrayIcon::MiddleClick:
        nCntrl->isAllNotesVisible() ? nCntrl->hideAll() : nCntrl->restoreAll();
        break;
    default:
        return;
    }
}

void Tray::on_UpdateNotesCount(const int & num)
{
    m_notes_count_lbl->setText(QString::number(num));
}
