#ifndef NOTE_H
#define NOTE_H

#define FOLD_TIME 200

#include <QtWidgets/QWidget>
#include <QImage>
#include "Skins/SkinsManager.h"

class QGraphicsOpacityEffect;
class QPropertyAnimation;
class QPushButton;
class EditField;
class QLabel;

namespace StatusMarks{
    static const QImage exclamationMark(":images/Marks/exclamation.png");
};

class Note : public QWidget
{
    Q_OBJECT
    Q_CLASSINFO("Role", "Note itself" )

public:
    Note(const QString &winDate = NULL, quint64 UID = 0, QWidget *parent = 0);
    ~Note();

public:
    QString      getDate() {  return currDate; }
    static qreal getOpacityLevel(){ return currOpacity; }

    QString      getNoteText() const;
    void         setNoteText(const QString&);

    QPoint getNotePos() const { return m_wnd_pos; }

    void validateGeometry();

    void setUID( quint64 UID );
    inline quint64 getUID() const { return mUID; }
    
    QString getSkinName() const;

public slots:
    void moveNote(QPoint coord);
    void smoothHide();
    void smoothShow();
    void incVisibleLevel();
    void decVisibleLevel();
    void setSkin(SkinPtr skin);
    void setOpacityLevel(qreal opacity);

protected:
     void initGui();
     QRect defaultGeometry();
    
     /*Events*/
     virtual void closeEvent(QCloseEvent *event);
     virtual void enterEvent(QEvent *);
     virtual void leaveEvent(QEvent *);
     virtual void paintEvent(QPaintEvent *event);
     virtual void keyPressEvent(QKeyEvent *event);
     virtual void moveEvent(QMoveEvent *event);
     virtual void resizeEvent(QResizeEvent *event);

signals:
    void closeNoteEvent_sig(Note*);
    void close_app_sig();
    void require_newNote_sig();

public:
    static qreal currOpacity;
protected:
    QPushButton *m_closeButt;
    QString currDate;
    EditField *m_textField;
    QLabel *m_pTitleLabel;
    QPropertyAnimation *m_InOutAnim;
    QPropertyAnimation *m_LblOpacAnim;
    QPropertyAnimation *m_closeBtnOpacAnim;
    QGraphicsOpacityEffect* m_OpacEff;
    QSize m_win_size;
    QPoint m_wnd_pos;
    QList<int> m_repaintIntervals;
    SkinPtr m_skin;
    quint64 mUID;
    bool m_AnimatingShowHide = false;
};

#endif // NOTE_H
