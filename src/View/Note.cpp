#include "Note.h"

#include <QtWidgets/QApplication>
#include <QtWidgets/QDesktopWidget>
#include <QtWidgets/QGraphicsDropShadowEffect>
#include <QFontDatabase>
#include <QPainter>
#include <QtWidgets/QLayout>
#include <QDate>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QLabel>
#include <QPropertyAnimation>
#include <QKeyEvent>

#ifdef Q_OS_WIN32
#include <windows.h>
#endif

#include "defs.h"
#include "Utils.h"
#include "MainFacade.h"
#include "NotesEng/NotesManager.h"
#include "View/EditField.h"

#define nMan NotesManager::getInstance()

qreal Note::currOpacity = 1.0;

//global vars
namespace{
    const QSize gDefSize(320, 240);
    constexpr quint8 gAnimateMax = 10;
}

Note::Note(const QString &winDate, quint64 UID, QWidget *parent)
    : QWidget(parent),
      currDate(winDate),
      m_win_size(gDefSize.width(), gDefSize.height()),
      mUID( UID )
{
    setObjectName("Note");
    
    initGui();
    show();

    connect(m_closeButt, &QPushButton::pressed, this, &Note::close);
    connect(m_textField, &EditField::sendCoord, this, &Note::moveNote);
    connect(m_textField, &EditField::resizeNote, this, (void(Note::*)(const QRect&))&Note::setGeometry);
    connect(m_textField, &EditField::resized, this, &Note::validateGeometry);
}

Note::~Note()
{
    destroy(true, true);
}

void Note::initGui(){
    static bool makeItOnce = false;
    if( !makeItOnce )
    {
        makeItOnce = true;
        //load fonts
        QFontDatabase::addApplicationFont(":/Fonts/Monaco.ttf");
        QFontDatabase::addApplicationFont(":/Fonts/SFDigital.ttf");
    }

    //text field
    m_textField = new EditField(this);
    m_textField->setFontFamily("Monaco");
    m_textField->setFontPointSize(10);
    m_textField->setAccessibleName("edit_field");

    //date label
    {
        m_pTitleLabel = new QLabel("Need upd", this);
        m_pTitleLabel->setAlignment(Qt::AlignLeft | Qt::AlignBottom);
        QFont font = m_pTitleLabel->font();

        //font
        font.setPointSize(14);
        font.setFamily("SF Digital Readout");
        m_pTitleLabel->setFont(font);
        m_pTitleLabel->setAccessibleName("date_lbl");
        //m_pTitleLabel->setStyleSheet("color: #ba7303;");
    }

    // shadow effects for whole note widget
    {
        QGraphicsDropShadowEffect *effect = new QGraphicsDropShadowEffect(this);
        effect->setBlurRadius(7);
        effect->setColor(QColor(0,0,0));
        effect->setOffset(5,5);
        m_textField->setGraphicsEffect(effect);
        m_textField->setStyleSheet("padding:8px;");
    }

    setGeometry(defaultGeometry());
    m_textField->setGeometry(defaultGeometry());

    // close btn
    m_closeButt = new QPushButton(this);
    m_closeButt->setFixedSize(19,15);
    m_closeButt->setIconSize(QSize(m_closeButt->size()));
    m_closeButt->setIcon(QIcon(":/images/close.png"));
    m_closeButt->setStyleSheet("background-color: transparent");
//  m_closeButt->hide();

    //layout
    QGridLayout *mnLayout = new QGridLayout(this);
    mnLayout->addWidget(m_pTitleLabel,0,0,Qt::AlignLeft);
    mnLayout->addWidget(m_closeButt,0,1,Qt::AlignRight | Qt::AlignBottom);
    mnLayout->addWidget(m_textField,1,0,1,2);
    mnLayout->setRowMinimumHeight(0, m_closeButt->height());
    mnLayout->setColumnMinimumWidth(1,3);
    mnLayout->setColumnStretch(0,1);
    mnLayout->setVerticalSpacing(3);
    setLayout(mnLayout);
    
    // show date in title
    if(currDate.isEmpty()){
        currDate = QDate::currentDate().toString("dd/MM/yyyy");
    }
    m_pTitleLabel->setText(currDate);
    
    // opacity effect for whole note
    m_OpacEff = new QGraphicsOpacityEffect(this);
    m_textField->setGraphicsEffect(m_OpacEff);
    
    //date label opacity effect
    auto lblOpacEff = new QGraphicsOpacityEffect(this);
    lblOpacEff->setOpacity(0);
    m_pTitleLabel->setGraphicsEffect(lblOpacEff);
    
    //opacity property anim for whole note
    m_InOutAnim = new QPropertyAnimation(m_OpacEff, "opacity", this);
    m_InOutAnim->setDuration(300);
    m_InOutAnim->setEasingCurve(QEasingCurve::Linear);
    setOpacityLevel(currOpacity);

    auto closeBtnOpacEff = new QGraphicsOpacityEffect(this);
    closeBtnOpacEff->setOpacity(0.1f);
    m_closeButt->setGraphicsEffect(closeBtnOpacEff);
    
    //opacity property anim for date label
    m_LblOpacAnim = new QPropertyAnimation(lblOpacEff, "opacity", this);

    //opacity property anim for close button
    m_closeBtnOpacAnim = new QPropertyAnimation(closeBtnOpacEff, "opacity", this);

    // window behavior and flags
#ifdef Q_OS_WIN
    setWindowModality(Qt::NonModal);
    setWindowFlags( Qt::Widget | Qt::FramelessWindowHint );
#else
    //TODO: Test this on Win and Mac
    setWindowFlags( Qt::SplashScreen | Qt::FramelessWindowHint );
#endif
    setAttribute(Qt::WA_TranslucentBackground );
}

void Note::closeEvent(QCloseEvent *event)
{ 
    if (sender() == m_closeButt){
        int t = 400;
        
        QEventLoop loop;
        
        m_InOutAnim->stop();
        this->setWindowOpacity(1);
        
        QPropertyAnimation *animation = new QPropertyAnimation(this, "windowOpacity", this);
        animation->setDuration(t);
        animation->setStartValue(1);
        animation->setEndValue(0.0);
        animation->setEasingCurve(QEasingCurve::Linear);
        animation->start(QAbstractAnimation::DeleteWhenStopped);
        QTimer::singleShot(t+1, &loop, SLOT(quit()));
        this->setEnabled(false);
        loop.exec();
    }
    
    emit closeNoteEvent_sig(this);
    QWidget::closeEvent(event);
}

void Note::validateGeometry()
{
    auto desktop = qApp->desktop();
    auto scrRect = desktop->screenGeometry(desktop->screenNumber(this));
    auto& geometry = this->geometry();

    if( !m_skin.isNull() )
    {
        auto size = geometry.size();
        //size validation
        if(size.width() > m_skin->GetMaxSize().width() )
            this->setGeometry( QRect(geometry.topLeft(), QSize(m_skin->GetMaxSize().width(), size.height())) );
        if(size.height() > m_skin->GetMaxSize().height())
            this->setGeometry(QRect(geometry.topLeft(), QSize(geometry.width(), m_skin->GetMaxSize().height())) );
    }

    //check whether it didn't go out of screen
    if( !scrRect.contains(this->geometry()) )
    {
        this->setGeometry(defaultGeometry());
    }
}

void Note::setUID(quint64 UID)
{
    if(mUID)
    {
        Q_ASSERT(!"Can not change UID, set it only once!");
        return;
    }

    mUID = UID;
}

QString Note::getSkinName() const
{
    return (m_skin.isNull() ? "" : m_skin->GetName());
}

QRect Note::defaultGeometry()
{
    //FIXME: reimplement me! Note::defaultGeometry()
    static int x = 0,y = 0;
    static int incX = 50, incY = 50;
    
    auto desktop = qApp->desktop();
    auto scrRect = desktop->screenGeometry(desktop->screenNumber(this));
    int ScreenWidth = scrRect.width();
    int ScreenHeight = scrRect.height();

    m_wnd_pos = QPoint(x+=incX, y+=incY);
    QRect rectForNote(m_wnd_pos, m_win_size);

    if(y >= ScreenHeight-rectForNote.height() || y <= 0){
       incY = incY * (-1);
    }

    if(x >= ScreenWidth-rectForNote.width() || x <= 0){
       incX = incX * (-1);
    }

    return rectForNote;
}

QString Note::getNoteText() const
{
    return m_textField->toHtml();
}

void Note::setNoteText(const QString &text)
{
    m_textField->setHtml(text);
}

void Note::setOpacityLevel(qreal opacity)
{
    currOpacity = opacity;
    m_OpacEff->setOpacity(opacity);
}

void Note::enterEvent(QEvent *e)
{   
    if (m_OpacEff->opacity() < qreal(1)){
        incVisibleLevel();
    }

    auto dur = 1300;
#if 0
    // no need to maintain deleting, auto deletes on each set
    QGraphicsDropShadowEffect *shadow_eff = new QGraphicsDropShadowEffect(m_pTitleLabel);
    shadow_eff->setBlurRadius(10);
    shadow_eff->setOffset(0);
    m_pTitleLabel->setGraphicsEffect(shadow_eff);

    connect(shadow_eff, &QGraphicsDropShadowEffect::destroyed, []{DEB << "destr";});

    auto col_eff = new QPropertyAnimation(shadow_eff, "color", this);
    col_eff->setDuration(dur1);
    col_eff->setEasingCurve(QEasingCurve::OutCubic);
    col_eff->setStartValue(QColor(qrand()%250, qrand()%250, qrand()%250));
    col_eff->setEndValue(QColor(qrand()%250, qrand()%250, qrand()%250));
    col_eff->start(QAbstractAnimation::DeleteWhenStopped);
#endif

    // Date label effect
    m_LblOpacAnim->stop();
    m_LblOpacAnim->setDuration(dur);
    m_LblOpacAnim->setEasingCurve(QEasingCurve::InQuad);
    m_LblOpacAnim->setStartValue(m_LblOpacAnim->currentValue().toInt());
    m_LblOpacAnim->setEndValue(1);
    m_LblOpacAnim->start();

    // close button effect
    m_closeBtnOpacAnim->stop();
    m_closeBtnOpacAnim->setDuration(dur/5);
    m_closeBtnOpacAnim->setEasingCurve(QEasingCurve::InQuad);
    m_closeBtnOpacAnim->setStartValue(m_closeBtnOpacAnim->currentValue().toInt());
    m_closeBtnOpacAnim->setEndValue(1);
    m_closeBtnOpacAnim->start();
    
//    m_closeButt->show();

    QWidget::enterEvent(e);
}

void Note::leaveEvent(QEvent *e)
{
    if (!m_textField->hasFocus())
        decVisibleLevel();

    QWidget::leaveEvent(e);
}

void Note::paintEvent(QPaintEvent *event)
{
//    TODO: Implement warning mark drawing here
//    QPainter painter(this);
//    painter.setPen(Qt::red);
//    painter.setFont(QFont("Arial", 30));
//    painter.drawText(rect(), Qt::AlignCenter, "TooSla (c)");

    QPainter painter(this);
    const QImage & img = StatusMarks::exclamationMark;

    QPoint drawPoint = {width() / 2 - img.width() / 2, (height() / 2) - img.height() / 2};
    painter.drawImage(drawPoint, img);
    
    QWidget::paintEvent(event);
}

void Note::keyPressEvent(QKeyEvent *event)
{
    switch(event->key())
    {
       case Qt::Key_W :
           if( event->modifiers() == Qt::ControlModifier )
               close();
           break;
       case Qt::Key_H :
           if( event->modifiers() == Qt::ControlModifier )
              smoothHide();
           break;
       case Qt::Key_Q :
           if( event->modifiers() == Qt::ControlModifier )
              emit close_app_sig();
           break;
       case Qt::Key_N :
           if( event->modifiers() == Qt::ControlModifier )
             emit require_newNote_sig();
           break;
       default:
           break;
    }
}

void Note::moveEvent(QMoveEvent *event)
{
    QWidget::moveEvent(event);
    if( !m_AnimatingShowHide && isVisible() )
        m_wnd_pos = this->pos();
}

void Note::resizeEvent(QResizeEvent *event)
{
    QWidget::resizeEvent(event);
    if( !m_AnimatingShowHide && isVisible() )
        m_win_size = size();
}

void Note::incVisibleLevel()
{
    if ( m_InOutAnim->currentValue().toFloat() < 1.f)
    {
        m_InOutAnim->stop();
        m_InOutAnim->setStartValue(currOpacity);
        m_InOutAnim->setEndValue(1);
        m_InOutAnim->start();
    }
}

void Note::decVisibleLevel()
{
    // edit field
    if (Note::currOpacity != 1.f){
        m_InOutAnim->stop();
        m_InOutAnim->setStartValue(m_InOutAnim->currentValue());
        m_InOutAnim->setEndValue(currOpacity);
        m_InOutAnim->start();
    }

    auto dur = 1500;

    // Date lbl dissapear
    m_LblOpacAnim->stop();
    m_LblOpacAnim->setDuration(dur);
    m_LblOpacAnim->setEasingCurve(QEasingCurve::Linear);
    m_LblOpacAnim->setStartValue(m_LblOpacAnim->currentValue().toInt());
    m_LblOpacAnim->setEndValue(0);
    m_LblOpacAnim->start();
    
    // hide close button
    m_closeBtnOpacAnim->stop();
    m_closeBtnOpacAnim->setDuration(dur/5);
    m_closeBtnOpacAnim->setEasingCurve(QEasingCurve::Linear);
    m_closeBtnOpacAnim->setStartValue(m_closeBtnOpacAnim->currentValue().toInt());
    m_closeBtnOpacAnim->setEndValue(0.1f);
    m_closeBtnOpacAnim->start();

//    m_closeButt->hide();
}

void Note::setSkin(SkinPtr skin)
{
    Q_ASSERT( !skin.isNull() );
    if( skin.isNull() )
        return;

    static const auto dSize =  size() - m_textField->size();
    if (skin->UseFixedSize()){
        m_win_size = skin->GetSize() + dSize; // what are whe need dSize here for???
        resize(m_win_size);
    }else{
        //temp solution, default size must be set only if skin doesn't have desireble one
        m_win_size = gDefSize; //TODO: Provide getDefaultSize() for each func separate Skin
        resize(m_win_size);
    }

    //animations
    foreach(int i, skin->GetImages().keys()){
        auto timer = skin->GetImages().value(i)->getUpdTimer();
        m_repaintIntervals.clear();
        if (timer->interval() > 0 && !m_repaintIntervals.contains(timer->interval())){
            m_repaintIntervals.append( timer->interval() );
            connect(timer, SIGNAL(timeout()), m_textField, SLOT(update()));
        }
    }

    //stylesheet
    this->setStyleSheet( skin->GetStyle() );
    
    m_skin = skin;
    m_textField->setSkin(skin);

    validateGeometry();
}

void Note::smoothHide()
{
    if ( isHidden() )
        return;

    // to temporary disable move/resize tracking
    m_AnimatingShowHide = true;
    static const auto t = FOLD_TIME;
    
    // geometry anim
    QPropertyAnimation* hideAnim = new QPropertyAnimation(this, "geometry", this);
    hideAnim->setDuration(t);
    hideAnim->setStartValue(geometry());
    hideAnim->setEndValue(HIDDEN_NOTE_POS);
    hideAnim->setEasingCurve(QEasingCurve::OutQuad);

    connect( hideAnim, &QPropertyAnimation::finished, [this]{ m_AnimatingShowHide = false; } );
    connect( hideAnim, &QPropertyAnimation::finished, this, &Note::hide );

    if (nMan->getNotesCount() <= gAnimateMax )
    {
        // put anim in loop to hide all notes consiquently
        QEventLoop loop;
        connect(hideAnim, &QPropertyAnimation::finished, &loop, &QEventLoop::quit);
        hideAnim->start(QAbstractAnimation::DeleteWhenStopped);
        loop.exec();
    }else{
        // animate all simultaneously
        setUpdatesEnabled(false);
        connect(hideAnim, &QPropertyAnimation::finished, [this](){ this->setUpdatesEnabled(true); } );
        hideAnim->start(QAbstractAnimation::DeleteWhenStopped);
    }
}

void Note::smoothShow()
{
    if ( isVisible() )
        return;
    
    m_AnimatingShowHide = true;
    show();

    // geometry anim
    QPropertyAnimation* showAnim = new QPropertyAnimation(this, "geometry", this);
    showAnim->setDuration(FOLD_TIME);
    showAnim->setStartValue(geometry());
    showAnim->setEndValue(QRect(m_wnd_pos, m_win_size));
    showAnim->setEasingCurve(QEasingCurve::OutQuad);

    connect( showAnim, &QPropertyAnimation::finished, [this]{ m_AnimatingShowHide = false; } );

    if (nMan->getNotesCount() > gAnimateMax )
    {
        setUpdatesEnabled(false);
        connect(showAnim, &QPropertyAnimation::finished, [this](){ this->setUpdatesEnabled(true); } );
    }

    showAnim->start(QAbstractAnimation::DeleteWhenStopped);
}

void Note::moveNote(QPoint coord)
{
    move(coord);
    m_wnd_pos = coord; // for further restoring
}

