#include "Properties.h"
//#include "Google/GooLogin.h"
#include "Skins/SkinsManager.h"

#include <QtWidgets/QLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSlider>
#include <QtWidgets/QComboBox>
#include <QSettings>
#include <QDir>
#include <QtWidgets/QApplication>
#include <QCloseEvent>
#include <QtWidgets/QPushButton>

#include <QDebug>

Properties::Properties( QWidget *parent):
    QWidget(parent)
{
    init();
}

void Properties::init()
{
    QGridLayout *mnLayout = new QGridLayout(this);
    skinCmbBox = new QComboBox;
    opacitySlider  = new QSlider(Qt::Horizontal);
    opacitySlider->setRange(20,100);
    QPushButton *okBtn = new QPushButton(trUtf8("LOC_APPLY"), this);
    QPushButton *cancelBtn = new QPushButton(trUtf8("LOC_CANCEL"), this);
    /* TODO: Login inside the browser
    QPushButton *autherizeBtn = new QPushButton(trUtf8("LOC_AUTH"));
    m_webView = new GooLogin(this);

    m_webView->resize(10,20);
//    m_weView->load(QUrl("http://www.google.com/"));
*/

    readSettings();

    mnLayout->addWidget(new QLabel(trUtf8("LOC_SET_SKIN")), 0, 0);
    mnLayout->addWidget(skinCmbBox,0,1);
    mnLayout->addWidget(new QLabel(trUtf8("LOC_OPACITY")), 1,0);
    mnLayout->addWidget(opacitySlider,2,0,1,2);
//    mnLayout->addWidget(m_webView,3,0); // uncomment when implementing google login
//    mnLayout->addWidget(autherizeBtn,4,0);
    mnLayout->addWidget(okBtn,5,0);
    mnLayout->addWidget(cancelBtn,5,1);

    //    window appearance set up
    setLayout(mnLayout);
    setWindowFlags(Qt::Dialog
                   | Qt::MSWindowsFixedSizeDialogHint
                   | Qt::WindowTitleHint
                   | Qt::WindowStaysOnTopHint);

    setWindowModality(Qt::WindowModal);
    setWindowTitle(trUtf8("LOC_PREFS"));

    connect(skinCmbBox, SIGNAL(activated(QString)), this, SLOT(skinChange(QString)));
    connect(opacitySlider, SIGNAL(valueChanged(int)), SLOT(opacityChange()));
    connect(okBtn, SIGNAL(clicked()), SLOT(saveChanges_and_close()));
    connect(cancelBtn, SIGNAL(clicked()), SLOT(discardChanges()));
}

void Properties::readSettings()
{
    QStringList listOfFiles = SkinsManager::GetInst()->GetAvailableSkinsList();
    foreach(const QString& string, listOfFiles){
        skinCmbBox->addItem(string);
    }
    
//    if(!m_settings)
//        return;

//    skinCmbBox->setCurrentIndex(skinCmbBox->findText(m_settings->value("Current Skin", "Default").toString(), Qt::MatchFixedString));

//    if(int opacity = (int)(m_settings->value("Opacity", 0).toReal() * 100.0)){
//        opacitySlider->setValue(opacity); //set opacity value
//    }else{
//        opacitySlider->setValue(100); //set default opacity value
//    }
}

void Properties::closeEvent(QCloseEvent *event)
{
    hide();
    event->ignore();
}

void Properties::skinChange(const QString skin)
{
    emit skinChanged(skin);
}

void Properties::discardChanges()
{//returns the values from registry
//    emit skinChanged(m_settings->value("Current Skin", "default").toString());
//    emit opacityChanged(m_settings->value("Opacity", 1.0).toReal() );
    close();
}

void Properties::opacityChange(){
    emit opacityChanged((qreal)opacitySlider->value() / 100.0);
}

void Properties::saveChanges_and_close() // save to registry and close
{
//    m_settings->setValue("Opacity", ((qreal)opacitySlider->value() / 100.0));
//    m_settings->setValue("Current Skin", skinCmbBox->currentText());
    close();
}
