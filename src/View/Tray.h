#ifndef TRAY_H
#define TRAY_H

#include <QtWidgets/QSystemTrayIcon>

class QLabel;

class Tray : public QSystemTrayIcon
{
    Q_OBJECT
public:
    explicit Tray(QObject *parent = 0);

public slots:
    void on_UpdateNotesCount(const int&);
private slots:
    void on_Tray_activated(QSystemTrayIcon::ActivationReason);

private: //members
    QSystemTrayIcon *trayIcon;
    QMenu* m_trayIconMenu;
    QLabel *m_notes_count_lbl;

private: //methods
    void setUpTray();
};

#endif // TRAY_H
