#ifndef EDITFIELD_H
#define EDITFIELD_H

#include <QTextEdit>

#include "Skins/SkinsManager.h"

class EditField : public QTextEdit
{
    Q_OBJECT
public:
    explicit EditField(QWidget *parent = 0);
    explicit EditField(const QString &text, QWidget *parent = 0);

    bool supportsResizing() const;

protected:
    void Init();
    QRect GetResizeBtnRect() const;

protected: //Events
    virtual void mousePressEvent(QMouseEvent *e);
    virtual void mouseReleaseEvent(QMouseEvent *e);
    virtual void mouseMoveEvent(QMouseEvent *e);
    virtual void leaveEvent(QEvent * event);
    virtual void focusOutEvent( QFocusEvent * e ); 
    virtual void paintEvent(QPaintEvent *event);

public slots:
    void setSkin(SkinPtr ptr);
    void ShowContextMenu( const QPoint& pos );
    
signals:
    void sendCoord(QPoint);
    void resizeNote(QRect);
    void resized();

private:
    bool m_bMidBtnPressed;
    bool m_dragResizingEnabled;
    bool m_resizeCursorActive;
    QPoint m_prev_pos;
    SkinPtr m_skin;
};

#endif // EDITFIELD_H
