#ifndef DEFS_H
#define DEFS_H

#include <QDebug>

#ifdef NETWORK_SET_INCL
   #include <QNetworkAccessManager>
   #include <QNetworkReply>
   #include <QNetworkRequest>
   #include <QEventLoop>
#endif

#define DEB qDebug()<<"[debug]: "
#define COMPANY_NAME "Toosla's software"
#define APP_NAME "Sticky Notes"

// COMMON VARIABLES
namespace SkinNames{
const char* const Current = "Current Skin";
const char* const Default = "default";
}

#endif // DEFS_H
