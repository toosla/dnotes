/*
 * Convinient interface for simplifying some common tasks
 */

#ifndef MAINFACADE_H
#define MAINFACADE_H

#include <QRect>

class QObject;

class MainFacade
{
public:
    MainFacade();
    
private:
    static QObject* m_trayPtr;
    
public:
    void initializeAppEngines();
    void createTrayCotrol();
    void startGtaskEng();
    static QRect getTrayGeometry();
};

#endif // MAINFACADE_H
