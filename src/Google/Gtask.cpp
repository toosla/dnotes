#include "Gtask.h"

//del it and enable macro when finish ------------------------
//#define NETWORK_SET_INCL
      #include <QNetworkAccessManager>
      #include <QNetworkReply>
      #include <QNetworkRequest>
      #include <QEventLoop>
      #include <QTcpSocket>
      #include <QHostAddress>
// -------------------------------------------------------------
#include <QTimer>

#include "../View/Note.h"
#include "../defs.h"
#include "GTaskLib.h"

Gtask* Gtask::m_pInstance = NULL;

Gtask::Gtask(QObject *parent):
    QThread(parent),
    m_timerSync(NULL),
    m_syncTime(-1),
    m_CheckInetConnectionTime(-1)
{
//    moveToThread(this);
}

Gtask * Gtask::getInstance(QObject *parent)
{
    if(!m_pInstance)
        m_pInstance = new Gtask(parent);
    return m_pInstance;
}

void Gtask::run()
{
#ifdef GTASK_LIB
     DEB << "Google task engine started!";
     
    // sync interval, account data etc.
    if (!validateSettings()){
        terminate();
    }

    m_GtaskLib = GTaskLib::getInstance();

    m_timerSync = new QTimer(this);
    m_timerSync->setInterval(m_syncTime);
    connect(m_timerSync, SIGNAL(timeout()), SLOT(Syncronize()));

    Syncronize();

    exec();
#endif
}

void Gtask::addNoteToSyncList(const Note* note)
{
    m_notesToSync.enqueue(note);
}

void Gtask::setSyncInterval(const int interval)
{
    m_syncTime = interval;
    m_timerSync->setInterval(interval);
}

int Gtask::getSyncInterval() const
{
    return m_syncTime;
}

void Gtask::setAccountData(QString login, QString password)
{
    if (login.isEmpty() || password.isEmpty()){
        m_account.AllIsSet = false;
        return;
    }else{
        m_account.login = login;
        m_account.password = password;
        m_account.AllIsSet = true;
    }
}

QPair<QString, QString> Gtask::getAccountData() const
{
    QPair<QString, QString> pair;

    pair.first = m_account.login;
    pair.second = m_account.password;

    return pair;
}

void Gtask::setAccessToken(QString &token)
{
    m_accessToken = token;
}

bool Gtask::Syncronize()
{
    bool isSuccessfull = false;
    m_timerSync->stop();
    // checking for internet connection
    {
        int tryCount = 0;
        while (!isActiveInetConnection() && ++tryCount){
            if(tryCount == 25){
                DEB << "I-net connection check failed after "<< tryCount << "retries\n"
                    << "idle for" << m_CheckInetConnectionTime/60 << "minutes";
                tryCount = 0;
                sleep(m_CheckInetConnectionTime);
            }
        }
    }

    DEB << "Start syncing";

    m_timerSync->start();
    return isSuccessfull;
}

bool Gtask::isActiveInetConnection()
{
    QTcpSocket *socket = new QTcpSocket;
    socket->connectToHost("google.com", 80);
    if (socket->waitForConnected(2000)){
        DEB << "I-net connecting established";
        return true;
    }else{
        DEB << "I-net connection failed.";
        return false;
    }
}

bool Gtask::validateSettings()
{
    if (!m_account.AllIsSet || m_accessToken.isEmpty()){
        DEB << "not all settings are set in AccountData. Gtask will terminate";
        return false;
    }

    if (m_syncTime == -1){
        m_syncTime = DefaultSyncTime;
    }

    if(m_CheckInetConnectionTime == -1){
        m_CheckInetConnectionTime = DefaultCheckInetConnectionTime;
    }

    return true;
}
