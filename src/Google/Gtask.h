#ifndef GTASK_H
#define GTASK_H

#include <QObject>
#include <QThread>
#include <QQueue>
#include <QDateTime>
#include <QPair>

class QTimer;
class GTaskLib;

class Note;

class Gtask : public QThread
{
    Q_OBJECT
public:
    static Gtask* getInstance(QObject *parent = 0);
    void run();
private: // will be a singletone
    static Gtask *m_pInstance;
    explicit Gtask(QObject *parent = 0);

signals:
    void SyncFinished(QDateTime);
    void SyncError(QString error);

public slots:
    void addNoteToSyncList(const Note*);

public:
    void setSyncInterval(const int);
    int  getSyncInterval() const;
    void setAccountData(QString login, QString password);
    QPair<QString, QString> getAccountData() const;
    void setAccessToken(QString &token);

private slots:
    bool Syncronize();
    bool isActiveInetConnection();

private: //members
    enum { // in sec
        DefaultSyncTime = 60 * 1000 * 3,
        DefaultCheckInetConnectionTime = 60 * 10
    };

    struct AccountData{
        AccountData(): AllIsSet(false){}

        QString login;
        QString password;
        bool AllIsSet;
    };

    QQueue<const Note*> m_notesToSync;
    QTimer *m_timerSync;
    int m_syncTime;
    int m_CheckInetConnectionTime;
    QString m_accessToken;
    AccountData m_account;
    GTaskLib* m_GtaskLib;

private: //methods
    bool validateSettings();

public slots:

};

#endif // GTASK_H
