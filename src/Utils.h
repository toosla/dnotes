#ifndef UTILS_H
#define UTILS_H

#define HIDDEN_NOTE_POS QRect(MainFacade::getTrayGeometry().x(), MainFacade::getTrayGeometry().y(), 0, 0)
#define TRAY_SUPPORTED QSystemTrayIcon::isSystemTrayAvailable()

#endif // UTILS_H
