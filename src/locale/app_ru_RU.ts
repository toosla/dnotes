<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>Properties</name>
    <message>
        <location filename="../View/Properties.cpp" line="29"/>
        <source>LOC_APPLY</source>
        <translation>Применить</translation>
    </message>
    <message>
        <location filename="../View/Properties.cpp" line="30"/>
        <source>LOC_CANCEL</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="../View/Properties.cpp" line="41"/>
        <source>LOC_SET_SKIN</source>
        <translation>Задать скин</translation>
    </message>
    <message>
        <location filename="../View/Properties.cpp" line="43"/>
        <source>LOC_OPACITY</source>
        <translation>Уровень прозрачности</translation>
    </message>
    <message>
        <location filename="../View/Properties.cpp" line="58"/>
        <source>LOC_PREFS</source>
        <translation type="unfinished">Настройки...</translation>
    </message>
</context>
<context>
    <name>Tray</name>
    <message>
        <location filename="../View/Tray.cpp" line="22"/>
        <source>LOC_NOTES</source>
        <translation>Заметки</translation>
    </message>
    <message>
        <location filename="../View/Tray.cpp" line="24"/>
        <source>LOC_RESTORE_ALL</source>
        <translation>Восстановить все</translation>
    </message>
    <message>
        <location filename="../View/Tray.cpp" line="25"/>
        <source>LOC_HIDE_ALL</source>
        <translation>Скрыть все</translation>
    </message>
    <message>
        <location filename="../View/Tray.cpp" line="26"/>
        <source>LOC_CLOSE_ALL</source>
        <translation>Закрыть все</translation>
    </message>
    <message>
        <location filename="../View/Tray.cpp" line="28"/>
        <source>LOC_HELP</source>
        <translation>Помощь</translation>
    </message>
    <message>
        <location filename="../View/Tray.cpp" line="32"/>
        <source>LOC_CREATE_NEW</source>
        <translation>Новая заметка</translation>
    </message>
    <message>
        <location filename="../View/Tray.cpp" line="34"/>
        <source>LOC_PREFS</source>
        <translation type="unfinished">Параметры...</translation>
    </message>
    <message>
        <location filename="../View/Tray.cpp" line="38"/>
        <source>LOC_QUIT</source>
        <translation>Выход</translation>
    </message>
</context>
</TS>
