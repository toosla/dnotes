<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>Properties</name>
    <message>
        <location filename="../View/Properties.cpp" line="29"/>
        <source>LOC_APPLY</source>
        <translation>Apply</translation>
    </message>
    <message>
        <location filename="../View/Properties.cpp" line="30"/>
        <source>LOC_CANCEL</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../View/Properties.cpp" line="41"/>
        <source>LOC_SET_SKIN</source>
        <translation>Choose skin</translation>
    </message>
    <message>
        <location filename="../View/Properties.cpp" line="43"/>
        <source>LOC_OPACITY</source>
        <translation>Opacity</translation>
    </message>
    <message>
        <location filename="../View/Properties.cpp" line="58"/>
        <source>LOC_PREFS</source>
        <translation type="unfinished">Options...</translation>
    </message>
</context>
<context>
    <name>Tray</name>
    <message>
        <location filename="../View/Tray.cpp" line="22"/>
        <source>LOC_NOTES</source>
        <translation>Notes</translation>
    </message>
    <message>
        <location filename="../View/Tray.cpp" line="24"/>
        <source>LOC_RESTORE_ALL</source>
        <translation>Reveal all</translation>
    </message>
    <message>
        <location filename="../View/Tray.cpp" line="25"/>
        <source>LOC_HIDE_ALL</source>
        <translation>Conceal all</translation>
    </message>
    <message>
        <location filename="../View/Tray.cpp" line="26"/>
        <source>LOC_CLOSE_ALL</source>
        <translation>Close all</translation>
    </message>
    <message>
        <location filename="../View/Tray.cpp" line="28"/>
        <source>LOC_HELP</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="../View/Tray.cpp" line="32"/>
        <source>LOC_CREATE_NEW</source>
        <translation>New note</translation>
    </message>
    <message>
        <location filename="../View/Tray.cpp" line="34"/>
        <source>LOC_PREFS</source>
        <translation type="unfinished">Options...</translation>
    </message>
    <message>
        <location filename="../View/Tray.cpp" line="38"/>
        <source>LOC_QUIT</source>
        <translation>Quit</translation>
    </message>
</context>
</TS>
