/*
* Controller between main tray/widget and NotesManager.
* Also implements common event reactions. And hold's few widgets on board.
*/
#ifndef NOTESCONTROLLER_H
#define NOTESCONTROLLER_H

#include <QObject>

#include "Utils.h"
#include "Skins/SkinsManager.h"

class Properties;
class Note;

class NotesController: public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(NotesController)
    Q_CLASSINFO("Role", "Unifies events interface and behaviour model.")
    
    NotesController(QObject *parent = NULL);
public:
    static NotesController* getInstance(QObject* = 0);
    
public:
    bool isAllNotesVisible();
    void setWidgetTopmost(QWidget*);
    void addNote();
    void connectNote(const Note *) const;
    void hideAll();
    void closeAll();
    void restoreAll();
    void setAllNotesTopmost();
    
signals:
    void notesQuantityChanged(int cur_num); // sends current quantity as param
    
public slots:
    void on_closeNote(Note*);
    void onExit();
    void on_settingsShow();
    void onCommonSkinChange(QString prevSkinName, SkinPtr skin);
    
private:
    static NotesController* m_self;
    
    Properties *m_properties;
};

#endif // NOTESCONTROLLER_H
