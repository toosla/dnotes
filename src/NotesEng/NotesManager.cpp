#include "NotesManager.h"

#include "View/Note.h"
#include "Skins/SkinsManager.h"
#include "defs.h"
#include "Utils.h"
#include "NotesController.h"
#include "MainFacade.h"
#include "Utils/DBHelper.h"

#include <QApplication>
#include <QDesktopWidget>

NotesManager* NotesManager::m_self = NULL;

#define skMan SkinsManager::GetInst()
#define nCntrl NotesController::getInstance()

static constexpr short cRemovedUndoMax( 5 );

#ifdef QT_DEBUG
static constexpr quint16 cAutoSaveInterval(5 * 1000); //msec
#else
static constexpr quint16 cAutoSaveInterval(5 * 60 * 1000); //msec
#endif

NotesManager::NotesManager(QObject *parent) :
    QObject(parent)
{
    //SQL database init
    mDataBase = new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE"));
    mDataBase->setDatabaseName(QCoreApplication::applicationDirPath() + "/notes_db.sqlite");

    if (!mDataBase->open())
    {
        Q_ASSERT(!"Failed to open the database");
        qDebug() << mDataBase->lastError().text();
        delete mDataBase;
        mDataBase = nullptr;
    }

    //Autosave timer
    {
        QTimer* autosaveTimer = new QTimer(this);
        autosaveTimer->setInterval( cAutoSaveInterval );
        connect( autosaveTimer, &QTimer::timeout, this, &NotesManager::saveNotes );
        autosaveTimer->start(cAutoSaveInterval);
    }
}

NotesManager *NotesManager::getInstance(QObject* parent)
{
    
    if (!m_self){
        m_self = new NotesManager(parent);
    }
    
    return m_self;
}

Note* NotesManager::createNote(const QString &text, const QString &noteDate, quint64 UID, bool addToDB)
{
    auto newNote = new Note(noteDate, UID);
    m_notesVect.append(newNote);
    
    if(!text.isEmpty())
        newNote->setNoteText(text);
    
    newNote->setSkin(skMan->GetCurrCommonSkin());

    if( addToDB )
        DBHelper::UpdateOrInsertNote(newNote, false/*insert new entry*/);
    
    return newNote;
}

void NotesManager::removeNote(Note *note)
{
    if( note->getUID() > 0 ) //means it's in the table
        DBHelper::RemoveNoteWithUID(note->getUID());

    //save it to for undoing
    if( DBHelper::CreateRemovedNotesTable() )
        DBHelper::AddJustRemovedNote( note );

    m_notesVect.remove(m_notesVect.indexOf(note));
    delete note;
}

void NotesManager::saveNotes() const
{
    if( !DBHelper::CreateNotesTable() )
        return;

    bool reportError = false;
    for(Note* note : m_notesVect){
        bool res = DBHelper::UpdateOrInsertNote(note, note->getUID() > 0 );
        if(!reportError && !res)
            reportError = true;
    }

    if (!reportError)
        DEB << "NotesManager::saveNotes() " << m_notesVect.size() << " notes saved";
}

void NotesManager::loadNotes()
{
    if( !DBHelper::CreateNotesTable() )
        return;

    QSqlQuery q;
    q.setForwardOnly(true);
    q.exec("SELECT * FROM " + QString(notesTableName));

    Q_ASSERT(q.isActive());
    if(!q.isActive())
    {
        DEB << q.lastError();
        return;
    }

    while( q.next() )
    {
        short inc = 0;
        quint64 UID = q.value(inc++).toLongLong();
        const QString date = q.value(inc++).toString();
        const QString text = q.value(inc++).toString();
        QPoint loc;
        loc.setX( q.value(inc++).toInt() );
        loc.setY( q.value(inc++).toInt() );
        QSize size;
        size.setHeight( q.value(inc++).toInt() );
        size.setWidth( q.value(inc++).toInt() );
        const bool isHidden = q.value(inc++).toBool();
        const QString skinName = q.value(inc++).toString();

        auto pNote = createNote(text, date, UID);
        nCntrl->connectNote(pNote);

        // pos/size
        pNote->resize( size );
        pNote->moveNote( loc );

        // hidden
        if ( isHidden ){
            pNote->setGeometry(HIDDEN_NOTE_POS);
            pNote->hide();
        }else{
            pNote->validateGeometry();
        }

        //skin
        Q_ASSERT(!skinName.isEmpty());
        if( !skinName.isEmpty() && skinName != skMan->GetCurrCommonSkinName() )
        {
            SkinPtr skin = skMan->GetSkinByName( skinName );
            Q_ASSERT( skin );
            if(skin)
                pNote->setSkin( skin );
        }
    }

    DEB << "NotesManager::loadNotes()" << getNotesCount() << " notes loaded";

    emit nCntrl->notesQuantityChanged(getNotesCount());
}

namespace{
constexpr char opacityPropName[] = "opacity";
constexpr char commonSkinPropName[] = "commonSkin";
}

PropertiesInfo NotesManager::readProperties() const
{
    PropertiesInfo info;

    if(!DBHelper::CreatePrefsTable())
        return info;

    //opacity
    if( DBHelper::fetchPref( opacityPropName, info.opacity ) )
        DEB << "Fetched opacity from SQL: " << info.opacity;
    else
        DEB << "Failed to fetch opacity from SQL";

    //common skin
    if( DBHelper::fetchPref( commonSkinPropName, info.commonSkinName ) )
        DEB << "Fetched common skin name from SQL: " << info.commonSkinName;
    else
        DEB << "Could fetch common skin name from SQL";

    return info;
}

void NotesManager::saveProperties() const
{
    if(!DBHelper::CreatePrefsTable())
        return;

    //opacity
    DBHelper::UpdatePref(opacityPropName, nullptr, &Note::currOpacity, nullptr );

    //common skin
    QString skin = skMan->GetCurrCommonSkinName();
    DBHelper::UpdatePref(commonSkinPropName, nullptr, nullptr, &skin );
}

void NotesManager::loadProperties()
{
    auto props = readProperties();

    //opacity
    if( props.opacity >= 0 )
        Note::currOpacity = props.opacity;

    //skin
    if( !props.commonSkinName.isEmpty() )
        skMan->SetCurrCommonSkin( props.commonSkinName );
    else
        skMan->SetCurrCommonSkin( SkinNames::Default );
}

const QVector<const Note *> NotesManager::getNotes() const
{
    QVector<const Note *> vecBuf;
    
    foreach(auto note, m_notesVect){
        vecBuf.append(note);
    }
    return vecBuf;
}
