/*
 * In charge of all notes logic related actions. 
 * Notes creating, Load/Saving, counting etc.
 */

#ifndef NOTESMANAGER_H
#define NOTESMANAGER_H

#include "NoteList.h"

#include <QObject>
#include <QVector>

class Note;
class QSqlDatabase;

struct PropertiesInfo
{
    QString commonSkinName;
    qreal opacity = -1.f;
};

class NotesManager : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(NotesManager)
    Q_CLASSINFO("Role", "Manages notes lifecycle, save/load and other logic.")
    
    friend class MainFacade;

    explicit NotesManager(QObject *parent = 0); // singleton

public:
    static NotesManager* getInstance(QObject* = 0);

    Note* createNote(const QString& = 0, const QString& = 0, quint64 = 0, bool addToDB = false);
    void removeNote(Note*);

    void saveNotes() const;

    PropertiesInfo readProperties() const;
    void saveProperties() const;

		inline int getNotesCount() const { return m_notesVect.size(); }

    const QVector<const Note *> getNotes() const;

private:
    void loadNotes();
    void loadProperties();

    static NotesManager* m_self;
//		QVector<Note*> m_notesVect;
		QVector<NoteList> m_noteLists;
    QSqlDatabase *mDataBase;
};

#endif // NOTESMANAGER_H
