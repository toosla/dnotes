/*
		Description: Note list represents unique collection of notes. Each note must belong to some list.
*/

#pragma once

#include <memory>

class NoteList
{
public:
	explicit NoteList( uint64_t UID ) noexcept;
	~NoteList() = default;

	//TODO: implement
	NoteList(const NoteList& other) noexcept = delete;
	NoteList(NoteList&& other) noexcept = delete;
	NoteList& operator=(const NoteList& other) noexcept = delete;
	NoteList& operator=(NoteList&& other) noexcept = delete;

	inline quint64 UID() const noexcept;

	inline QString name() const noexcept;
	inline void setName( const QString& name ) noexcept;

	inline QVector<const Note *> getNotes() const noexcept;
	inline size_t count() noexcept;

private:
	friend class NotesManager;
	inline const QVector<Note*>& getNotesUnsafely() const noexcept;

	class impl;
	std::unique_ptr<impl> p_impl;

};
