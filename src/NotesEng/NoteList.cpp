#include "NoteList.h"

/*
 * -===========================================-
 * NoteList::impl internal implemenation class.
 * -===========================================-
 */

class NoteList::impl
{
public:


protected:

};

/*
 * -===========================================-
 * NoteList class.
 * -===========================================-
 */

quint64 NoteList::UID() const
{
	static_assert(false, "implement");
}

QString NoteList::name() const
{
	static_assert(false, "implement");
}

void NoteList::setName(const QString &name)
{
	static_assert(false, "implement");
}

QVector<const Note *> NoteList::getNotes() const
{
	static_assert(false, "implement");
}

size_t NoteList::count()
{
	static_assert(false, "implement");
}
