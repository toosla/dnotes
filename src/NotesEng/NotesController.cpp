#include "NotesController.h"

#include "NotesEng/NotesManager.h"
#include "View/Note.h"
#include "View/Properties.h"

#ifdef Q_OS_WIN32
#include <windows.h>
#endif

#include <QApplication>

#define nMan NotesManager::getInstance()
#define skMan SkinsManager::GetInst()

NotesController* NotesController::m_self = NULL;

NotesController::NotesController(QObject *parent) :
    QObject(parent)
{
    m_properties = new Properties;
    
    // CONNECTIONS 
    connect(m_properties, &Properties::skinChanged, skMan, &SkinsManager::SetCurrCommonSkin);
    connect(skMan, &SkinsManager::CommonSkinChanged, this, &NotesController::onCommonSkinChange);
}

NotesController *NotesController::getInstance(QObject *parent)
{
    if (!m_self){
        m_self = new NotesController(parent);
    }
    
    return m_self;
}

bool NotesController::isAllNotesVisible()
{
    foreach(auto note, nMan->getNotes()){
        if (note->isHidden()){
            return false;
        }
    }
    return true;
}

void NotesController::setWidgetTopmost(QWidget *wnd)
{
#ifdef Q_OS_WIN32
    // HACK: bringing window to top
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // void QWidget::activateWindow ()
    // ...
    // On Windows, if you are calling this when the application is not currently
    // the active one then it will not make it the active window. It will change
    // the color of the taskbar entry to indicate that the window has changed in
    // some way. This is because Microsoft do not allow an application to
    // interrupt what the user is currently doing in another application.
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // This hack does not give the focus to the app but brings it to front so
    // the user sees it.
    ::SetWindowPos((HWND)wnd->effectiveWinId(), HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
    ::SetWindowPos((HWND)wnd->effectiveWinId(), HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
    // HACK END
    
    wnd->raise();
    wnd->show();
    wnd->activateWindow();
#else
//    TODO: implement linux version
    Q_UNUSED(wnd);
#endif
}

void NotesController::on_closeNote(Note *note)
{
    nMan->removeNote(note);
    emit notesQuantityChanged(nMan->getNotesCount());
}

void NotesController::addNote()
{
    auto pNote = nMan->createNote("", "", 0, true);
    connectNote(pNote);
    
    emit notesQuantityChanged(nMan->getNotesCount());
}

void NotesController::connectNote(const Note *pNote) const
{
    connect(pNote, &Note::closeNoteEvent_sig, this, &NotesController::on_closeNote);
    connect(m_properties, &Properties::opacityChanged, pNote, &Note::setOpacityLevel);
    connect(pNote, &Note::close_app_sig, this, &NotesController::onExit);
    connect(pNote, &Note::require_newNote_sig, this, &NotesController::addNote);
}

void NotesController::onCommonSkinChange(QString prevSkinName, SkinPtr skin)
{
    foreach(Note* note, nMan->getNotesUnsafely())
    {
        if( note->getSkinName() == prevSkinName || prevSkinName.isEmpty() )
            note->setSkin(skin);
    }
}

void NotesController::hideAll()
{
    foreach(auto note, nMan->getNotesUnsafely()){
        note->smoothHide();
    }
}

void NotesController::restoreAll()
{
    foreach(auto note, nMan->getNotesUnsafely()){
        note->smoothShow();
    }
}

void NotesController::onExit()
{
    nMan->saveNotes();
    nMan->saveProperties();
    qApp->quit();
}

void NotesController::closeAll()
{
    foreach(auto note, nMan->getNotesUnsafely()){
        note->close();
    }
}

void NotesController::setAllNotesTopmost()
{
    foreach(auto note, nMan->getNotesUnsafely()){
        if (!note->isHidden()){
            setWidgetTopmost(note);
        }
    }
}

void NotesController::on_settingsShow()
{
    m_properties->show();
}
