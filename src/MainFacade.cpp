#include "MainFacade.h"

#include <QSystemTrayIcon>
#include <QObject>
#include <QSettings>
#include <QtWidgets/QWidget>

#include "View/Tray.h"
#include "NotesEng/NotesManager.h"
#include "NotesEng/NotesController.h"
#include "Skins/SkinsManager.h"
#include "Google/Gtask.h"

#include "defs.h"
#include "Utils.h"

QObject* MainFacade::m_trayPtr = NULL;

MainFacade::MainFacade()
{
}

void MainFacade::initializeAppEngines()
{
    auto nMan = NotesManager::getInstance();
    auto skMan = SkinsManager::GetInst();
    auto cntrler = NotesController::getInstance();
//    auto gtask = Gtask::getInstance();
    
    Q_UNUSED(skMan);
    Q_UNUSED(cntrler);

    //INITS
    nMan->loadProperties();
    nMan->loadNotes();
}

void MainFacade::createTrayCotrol()
{
    if ( TRAY_SUPPORTED ){
        auto tray = new Tray;
        tray->show();

        m_trayPtr = tray;
    }
    //TODO: tray replacing widget initalize here
}

void MainFacade::startGtaskEng()
{
    //TODO:  Set all parametres in GtaskEng Object before run it - read them from QSettings,
    //    threre are refresh token, sync timings, etc 

    //    m_pGtaskEng->start();
}

QRect MainFacade::getTrayGeometry()
{
    Q_ASSERT(m_trayPtr);
    
    if ( TRAY_SUPPORTED ){
        return (qobject_cast<QSystemTrayIcon *>(m_trayPtr))->geometry();
    }else{
        return (qobject_cast<QWidget *>(m_trayPtr))->geometry();
    }
}
