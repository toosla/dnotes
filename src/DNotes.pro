#-------------------------------------------------
#
# Project created by QtCreator 2011-07-11T22:21:50
#
#-------------------------------------------------

QMAKE_CXXFLAGS += -std=gnu++17
QT       += core gui widgets sql
#QT       += network webkit webkitwidgets

TARGET = DNotes
TEMPLATE = app

DESTDIR = "../dnotes/bin/"

SOURCES += main.cpp\
    View/Note.cpp \
    View/Tray.cpp \
    View/Properties.cpp \
#    Google/Gtask.cpp \
    Skins/Skin.cpp \
#    Google/GooLogin.cpp \
    Skins/SkinsManager.cpp \
    Skins/Image.cpp \
    NotesEng/NotesManager.cpp \
    NotesEng/NotesController.cpp \
    MainFacade.cpp \
    View/EditField.cpp \
    Utils/RunGuard.cpp \
    Utils/DBHelper.cpp \
    NotesEng/NoteList.cpp

HEADERS  += View/Note.h \
    View/Tray.h \
    View/Properties.h \
#    Google/Gtask.h \
    defs.h \
    Skins/Skin.h \
#    Google/GooLogin.h \
    Skins/SkinsManager.h \
    Skins/Image.h \
    NotesEng/NotesManager.h \
    NotesEng/NotesController.h \
    MainFacade.h \
    Utils.h \
    View/EditField.h \
    ToDoList.h \
    Utils/RunGuard.h \
    Utils/DBHelper.h \
    NotesEng/NoteList.h

RESOURCES += \
    resources/files.qrc

TRANSLATIONS = locale/app_en_US.ts \
               locale/app_ru_RU.ts


#LIBS += ../bin/GTaskLib.dll
#+LIBS += -L../bin/ -lGTaskLib

#INCLUDEPATH = ../GTaskLib




